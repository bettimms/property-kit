//
//  AddPropertyTableViewController.swift
//  Property Kit
//
//  Created by Betim S on 1/10/15.
//  Copyright (c) 2015 Betim Sejdiu. All rights reserved.
//

import UIKit

class AddPropertyTableViewController: UITableViewController,AddResidenceDelegate
{
    
    var propertyTableViewController:PropertyTableViewController?
    
    var country:Country?
    
    @IBOutlet weak var propertyNameTextField: UITextField!
    @IBOutlet weak var propertyNumberTextField: UITextField!
    @IBOutlet weak var propertyAgeTextField: UITextField!
    @IBOutlet weak var locationTextField: UITextField!
    
    var addResidenceDelegate:AddResidenceDelegate?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        //        self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelAndDismissAddPropertyModal(sender: AnyObject)
    {
        //        dataContext.property.delete()
        //        if dataContext.save()
        //        {
        //            println("Saved")
        //        }
        
        if let prop = dataContext.property.filterBy(attribute: "number",value:3).first()
        {
//            println(prop.city.name)
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func saveAndDismissPropertyModal(sender: AnyObject)
    {
        let property = dataContext.property.createEntity()
        
        if propertyNumberTextField.text != "" && propertyNumberTextField.text != ""
        {
            
            property.name = propertyNameTextField!.text
            property.number = propertyNumberTextField.text.toInt()!
            property.age = propertyAgeTextField!.text.toInt()!
            
            let city = dataContext.city.createOrGetFirstEntity(whereAttribute: "name", isEqualTo: "Dubai")
            property.city = city
            
            if dataContext.save()
            {
                println("Property Saved")
                
                propertyTableViewController?.reloadData()
            }
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: - AddResidenceDelegate implementation methods
    func didSelectCountry(country: Country)
    {
        self.country = country
        
        locationTextField.text = country.name
        
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "locationIdentifier"
        {
            let locationTableViewController = segue.destinationViewController as LocationTableViewController
            locationTableViewController.addResidenceDelegate = self
            
            locationTableViewController.selectedCountry = self.country
        }
        
    }
    
    
    
    // MARK: - Table view data source
    /*
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
    // #warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1
    }
    
    
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
    // #warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 10
    }
    
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
    let cell = tableView.dequeueReusableCellWithIdentifier("propertyCell", forIndexPath: indexPath) as UITableViewCell
    
    // Configure the cell...
    cell.textLabel!.text = "SIT Tower"
    return cell
    }
    */
    
    
    /*
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
    let cell = tableView.cellForRowAtIndexPath(indexPath)
    cell?.accessoryType = UITableViewCellAccessoryType.Checkmark
    }
    */
    
    /*
    override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath)
    {
    let cell = tableView.cellForRowAtIndexPath(indexPath)
    cell?.accessoryType = UITableViewCellAccessoryType.None
    }
    */
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    
    
    
    
}
