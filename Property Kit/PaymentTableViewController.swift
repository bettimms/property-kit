//
//  AddPaymentTableViewController.swift
//  Property Kit
//
//  Created by Betim S on 1/17/15.
//  Copyright (c) 2015 Betim Sejdiu. All rights reserved.
//

import UIKit

class PaymentTableViewController: UITableViewController
{
    
    var addResidenceDelegate:AddResidenceDelegate?
    
    let currencyArray = ["AED", "USD", "EUR", "CHF", "KSR","TRL"]
    
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var numberOfChequesTextField: UITextField!
    @IBOutlet weak var agencyFeeTextField: UITextField!
    @IBOutlet weak var depositAmountTextField: UITextField!
    
    
    @IBOutlet weak var noOfChequesStepper: UIStepper!
    @IBOutlet weak var currencyPickerView: UIPickerView!
    
    var currency:String = "AED"
    var price:String?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        priceTextField.text = price
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(true)
        
        //Reload selection from AddResidenceTVC
        for item in 0...currencyArray.count - 1
        {
            if currencyArray[item] == currency
            {
                currencyPickerView.selectRow(item, inComponent: 0, animated: true)
            }
        }
        
        
    }
    
    override func viewWillDisappear(animated: Bool)
    {
        super.viewWillDisappear(true)
        
        
        var payment:Payment = dataContext.payment.createEntity()
        payment.price = priceTextField.text
        
        if !numberOfChequesTextField.text.isEmpty
        {
            payment.numberOfCheques = numberOfChequesTextField.text.toInt()!
            
        }
        if !agencyFeeTextField.text.isEmpty
        {
            payment.agencyFee = agencyFeeTextField.text.toInt()!
            
        }
        if !depositAmountTextField.text.isEmpty
        {
            payment.deposit = depositAmountTextField.text.toInt()!
        }
          
        if !payment.price.isEmpty
        {
            payment.currency = !currency.isEmpty ? currency : "AED"

            addResidenceDelegate?.didSelectPayment!(payment)
        }
    }
    
    
    @IBAction func numberOfCheques(sender: AnyObject)
    {
        
        numberOfChequesTextField.text = String(Int(noOfChequesStepper.value))
    }
    
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return currencyArray.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String!
    {
        
        return currencyArray[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        currency = currencyArray[row]
    }
    
    
    @IBAction func cancelAndDismissAddPaymentModal(sender: AnyObject)
    {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func saveAndDismissAddPaymentModal(sender: AnyObject)
    {
        //        let property = dataContext.property.createEntity()
        //
        //        if propertyNumberTextField.text != "" && propertyNumberTextField.text != ""
        //        {
        //
        //            property.name = propertyNameTextField!.text
        //            property.number = propertyNumberTextField.text.toInt()!
        //            property.age = propertyAgeTextField!.text.toInt()!
        //
        //            let city = dataContext.city.createOrGetFirstEntity(whereAttribute: "name", isEqualTo: "Dubai")
        //            property.city = city
        //
        //            if dataContext.save()
        //            {
        //                println("Saved")
        //            }
        //        }
        
        
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    // MARK: - Table view data source
    /*
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
    // #warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
    // #warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0
    }
    */
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as UITableViewCell
    
    // Configure the cell...
    
    return cell
    }
    */
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    }
    */
    
}
