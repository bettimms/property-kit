//
//  AddResidenceTableViewController.swift
//  Property Kit
//
//  Created by Betim S on 1/10/15.
//  Copyright (c) 2015 Betim Sejdiu. All rights reserved.
//

import UIKit

class AddResidenceTableViewController: UITableViewController,AddResidenceDelegate
{
    
    var residenceTableViewController:ResidenceTableViewController?
    
    var areaUnit:AreaUnit?
    var facilityList:[Facility] = []
    var person:Person?
    var property:Property?
    var residenceType:ResidenceType?
    var payment:Payment?
    
    
    
    
    //Mandatory fields
    @IBOutlet weak var resDescriptionTextField: UITextField!
    @IBOutlet weak var resAreaTextField: UITextField!
    @IBOutlet weak var resTypeTextField: UITextField!
    @IBOutlet weak var resPersonTextField: UITextField!
    @IBOutlet weak var resStatusSegment: UISegmentedControl!
    @IBOutlet weak var resPriceTextField: UITextField!
    
    @IBOutlet weak var resCurrencyLabel: UILabel!
    
    //Optional fields
    @IBOutlet weak var resNumberTextField: UITextField!
    @IBOutlet weak var resPropertyNameTextField: UITextField!
    
    @IBOutlet weak var resKitchenTypeSegment: UISegmentedControl!
    @IBOutlet weak var resNoOfBathroomsTextField: UITextField!
    @IBOutlet weak var resNoOfLoundariesTextField: UITextField!
    @IBOutlet weak var resNoOfStoresTextField: UITextField!
    @IBOutlet weak var resIsFurnishedSwitch: UISwitch!
    @IBOutlet weak var resFacilitiesTextField: UITextField!
    
    
    
    
    //Steppers
    @IBOutlet weak var noOfBathroomsStepper: UIStepper!
    @IBOutlet weak var noOfLoundariesStepper: UIStepper!
    @IBOutlet weak var noOfStoresStepper: UIStepper!
    
    let tempObjects = NSUserDefaults()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    
    
    func saveResidence()
    {
        var residence = dataContext.residence.createEntity()
        
        let area = NSString(string: resAreaTextField.text)
        residence.area =  NSNumber(double: area.doubleValue)
        
        residence.isFurnished = resIsFurnishedSwitch.on
        residence.kitchenType = resKitchenTypeSegment.titleForSegmentAtIndex(resKitchenTypeSegment.selectedSegmentIndex)!
        residence.number = resNumberTextField.text
        
        let baths = NSString(string: resNoOfBathroomsTextField.text)
        residence.numberOfBathrooms =  NSNumber(double: baths.doubleValue)
        
        let loundaries = NSString(string: resNoOfLoundariesTextField.text)
        residence.numberOfLoundaries =  NSNumber(double: loundaries.doubleValue)
        
        let stores = NSString(string: resNoOfStoresTextField.text)
        residence.numberOfStoreRooms =  NSNumber(double: stores.doubleValue)
        
        residence.referenceId = "AX-B115"
        residence.residenceDescription = resDescriptionTextField.text
        residence.residenceStatus = resStatusSegment.titleForSegmentAtIndex(resStatusSegment.selectedSegmentIndex)!
        residence.residenceType = self.residenceType!
        residence.areaUnit = self.areaUnit!
        
        residence.facility = NSSet(array: facilityList)
        residence.person = self.person!
        residence.property = self.property!
        residence.dateAdded = NSDate()
        residence.payment = self.payment!
        
        
        
        if dataContext.save()
        {
            println("Residence saved successfully")
            
        }
        
    }
    
    @IBAction func saveAndDismissAddResidenceModal(sender: AnyObject)
    {
        saveResidence()
        residenceTableViewController?.reloadData()
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func dismissAddResidenceModal(sender: AnyObject)
    {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - AddResidenceDelegate Implementation
    
    func didSelectArea(area: String, areaUnit: AreaUnit)
    {
        self.areaUnit = areaUnit
        resAreaTextField.text = area
        
    }
    
    func didSelectResidenceType(residenceType:ResidenceType)
    {
        self.residenceType = residenceType
        resTypeTextField.text! = residenceType.name
    }
    
    func didSelectPerson(person: Person)
    {
        self.person = person
        resPersonTextField.text = person.firstName
    }
    
    func didSelectPayment(payment:Payment)
    {
        self.payment = payment
        
        resPriceTextField.text = payment.price
        resCurrencyLabel.text = payment.currency
    }
    
    func didSelectProperty(property: Property)
    {
        self.property = property
        resPropertyNameTextField.text = property.name
    }
    
    
    // MARK: - Action methods
    func didSelectFacilities(facilities:[Facility])
    {
        var facilityListString = ""
        
        if facilities.count > 0
        {
            for i in 0...facilities.count - 1
            {
                if i != facilities.count - 1
                {
                    facilityListString += "\(facilities[i].name), "
                }
                else
                {
                    facilityListString += "\(facilities[i].name)"
                }
                
            }
            
        }
        
        self.facilityList = facilities
        resFacilitiesTextField.text = facilityListString
    }
    
    
    //Segmented controls (Actions)
    @IBAction func residenceType(sender: AnyObject)
    {
        resStatusSegment.titleForSegmentAtIndex(resStatusSegment.selectedSegmentIndex)
    }
    
    @IBAction func kitchenType(sender: AnyObject)
    {
        resKitchenTypeSegment.titleForSegmentAtIndex(resKitchenTypeSegment.selectedSegmentIndex)
    }
    
    
    //Stepper controls (Actions)
    @IBAction func numberOfBathrooms(sender: AnyObject)
    {
        resNoOfBathroomsTextField.text = String(Int(noOfBathroomsStepper.value))
    }
    
    @IBAction func numberofLoundaries(sender: AnyObject)
    {
        resNoOfLoundariesTextField.text = String(Int(noOfLoundariesStepper.value))
    }
    
    @IBAction func numberOfStores(sender: AnyObject)
    {
        resNoOfStoresTextField.text = String(Int(noOfStoresStepper.value))
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "areaIdentifier"
        {
            let addAreaTableViewController = segue.destinationViewController as AreaTableViewController
            addAreaTableViewController.addResidenceDelegate = self
            
            addAreaTableViewController.areaValue = resAreaTextField.text!
            
            addAreaTableViewController.areaUnitName = areaUnit?.name
            
        }
        else if segue.identifier == "residenceTypeIdentifier"
        {
            let residenceTypeTableViewController = segue.destinationViewController as ResidenceTypeTableViewController
            residenceTypeTableViewController.addResidenceDelegate = self
            
            residenceTypeTableViewController.selectedResidenceType = self.residenceType
        }
        else if segue.identifier == "personIdentifier"
        {
            let personTableViewController = segue.destinationViewController as PersonTableViewController
            personTableViewController.addResidenceDelegate = self
            
            personTableViewController.selectedPerson = self.person
            
        }
        else if segue.identifier == "paymentIdentifier"
        {
            let paymentTableViewController = segue.destinationViewController as PaymentTableViewController
            paymentTableViewController.addResidenceDelegate = self
            
            paymentTableViewController.price = resPriceTextField.text
            paymentTableViewController.currency = resCurrencyLabel.text!
            
        }
        else if segue.identifier == "propertyIdentifier"
        {
            let propertyTableViewController = segue.destinationViewController as PropertyTableViewController
            propertyTableViewController.addResidenceDelegate = self
            
            propertyTableViewController.selectedProperty = self.property
        }
        else if segue.identifier == "facilityIdentifier"
        {
            let facilityTableViewController = segue.destinationViewController as FacilitiesTableViewController
            facilityTableViewController.addResidenceDelegate = self
            
            facilityTableViewController.selectedFacilities = facilityList
            
            
            
        }
        
        
    }
    
}
