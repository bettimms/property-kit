//
//  DataContext.swift
//  Property Kit
//
//  Created by Betim S on 1/6/15.
//  Copyright (c) 2015 Betim Sejdiu. All rights reserved.
//

import Foundation

let dataContext = DataContext()!

final class DataContext: Context
{
    var residence:Table<Residence> { return Table<Residence>(context: self) }
    var country:Table<Country> { return Table<Country>(context: self) }
    var city:Table<City> { return Table<City>(context: self) }
    var callType:Table<CallType> { return Table<CallType>(context: self) }
    var property:Table<Property> { return Table<Property>(context: self) }
    var facility:Table<Facility> { return Table<Facility>(context: self) }
    var residenceType:Table<ResidenceType> { return Table<ResidenceType>(context: self) }
    var personType:Table<PersonType> { return Table<PersonType>(context: self) }
    var person:Table<Person> { return Table<Person>(context: self) }
    var areaUnit:Table<AreaUnit> { return Table<AreaUnit>(context: self) }
    var payment:Table<Payment> { return Table<Payment>(context: self) }

}

