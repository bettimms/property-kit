//
//  AddAreaTableViewController.swift
//  Property Kit
//
//  Created by Betim S on 1/17/15.
//  Copyright (c) 2015 Betim Sejdiu. All rights reserved.
//

import UIKit


//TODO make areaUnit selected on picker on viewDidLoad once it's chosen


class AreaTableViewController: UITableViewController
{
    var areaUnitArray = []
    
    @IBOutlet weak var areaTextField: UITextField!
    var addResidenceDelegate:AddResidenceDelegate?
    
    @IBOutlet weak var areaPickerView: UIPickerView!
    
    var areaUnitName:String?
    var areaValue:String = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        areaTextField.becomeFirstResponder()
        areaTextField.text = areaValue
        
        
//        dataContext.reset()
        areaUnitArray = dataContext.areaUnit.toArray()
        
        
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(true)
        
        //Reload selection from AddResidenceTVC
        for item in 0...areaUnitArray.count - 1
        {
            if areaUnitArray[item].name == areaUnitName
            {
                areaPickerView.selectRow(item, inComponent: 0, animated: true)
                areaPickerView.selectRow(item, inComponent: 1, animated: true)
            }
        }
        
        
    }
    
    override func viewWillDisappear(animated: Bool)
    {
        super.viewWillDisappear(true)
        
        let selectedAreaUnit = (areaUnitArray[areaPickerView.selectedRowInComponent(0)] as AreaUnit)
        
        addResidenceDelegate?.didSelectArea!(areaTextField.text, areaUnit:selectedAreaUnit )
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int
    {
        return 2
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return areaUnitArray.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String!
    {
        
        
        if component == 0
        {
            return areaUnitArray[row].name
        }
        else
        {
            let area = areaUnitArray[row] as AreaUnit
            return area.symbol
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        pickerView.selectRow(row, inComponent: 0, animated: true)
        pickerView.selectRow(row, inComponent: 1, animated: true)
        
    }
    
    
    // MARK: - Table view data source
    /*
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
    // #warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
    // #warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0
    }
    */
    
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as UITableViewCell
    
    // Configure the cell...
    
    return cell
    }
    */
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    }
    */
    
}
