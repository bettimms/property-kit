//
//  DataHelper.swift
//  Property Kit
//
//  Created by Betim S on 1/10/15.
//  Copyright (c) 2015 Betim Sejdiu. All rights reserved.
//

import Foundation


class DataHelper
{
    class func insertStaticData()
    {
        DataHelper.insertCountries()
        DataHelper.inserCallTypes()
        DataHelper.insertProperties()
        DataHelper.insertFacilities()
        DataHelper.insertResidenceTypes()
        DataHelper.insertPersonTypes()
        DataHelper.insertPersons()
        DataHelper.insertAreaUnit()
    }
    
    class func insertCountries()
    {
        
        if dataContext.country.count() == 0
        {
            DataManager.getTopAppsDataFromFileWithSuccess("countries", { (data) -> Void in
                let json = JSON(data: data)
                
                for index in 0..<json.count
                {
                    var country = dataContext.country.createEntity()
                    
                    country.name = json[index]["name"].string!
                    country.code = json[index]["code"].string!
                    
                    //                println(country.name)
                }
                
                if dataContext.save()
                {
                    println("Saved successfully")
                }
                
            })
        }
        return
    }
    
    class func insertProperties()
    {
        
        if dataContext.property.count() == 0
        {
            DataManager.getTopAppsDataFromFileWithSuccess("properties", { (data) -> Void in
                let json = JSON(data: data)
                
                for index in 0..<json.count
                {
                    var property = dataContext.property.createEntity()
                    
                    property.name = json[index]["name"].string!
                    property.number = json[index]["number"].intValue
                    property.age = json[index]["age"].intValue
                    
                    let city = dataContext.city.createOrGetFirstEntity(whereAttribute: "name", isEqualTo: "Dubai")
                    property.city = city
                }
                
                if dataContext.save()
                {
                    println("Property Saved successfully")
                }
                
            })
        }
        return
    }
    
    class func inserCallTypes()
    {
        if dataContext.callType.count() == 0
        {
            DataManager.getTopAppsDataFromFileWithSuccess("callType", { (data) -> Void in
                let json = JSON(data: data)
                
                for index in 0..<json.count
                {
                    var callType = dataContext.callType.createEntity()
                    
                    callType.name = json[index]["name"].string!
                    callType.callDescription = json[index]["callDescription"].string!
                    
                }
                
                if dataContext.save()
                {
                    println("CallTypes Saved successfully")
                }
                
            })
        }
        
        return
    }
    
    class func insertFacilities()
    {
        
        if dataContext.facility.count() == 0
        {
            DataManager.getTopAppsDataFromFileWithSuccess("facilities", { (data) -> Void in
                let json = JSON(data: data)
                
                for index in 0..<json.count
                {
                    var facility = dataContext.facility.createEntity()
                    
                    facility.name = json[index]["name"].string!

                }
                
                if dataContext.save()
                {
                    println("Facility Saved successfully")
                }
                
            })
        }
        return
    }
    
    class func insertResidenceTypes()
    {
        dataContext.reset()
        
        if dataContext.residenceType.count() == 0
        {
            DataManager.getTopAppsDataFromFileWithSuccess("residenceTypes", { (data) -> Void in
                let json = JSON(data: data)
                
                for index in 0..<json.count
                {
                    var residenceType = dataContext.residenceType.createEntity()
                    
                    residenceType.name = json[index]["name"].string!
                    
                }
                
                if dataContext.save()
                {
                    println("ResidenceTypes Saved successfully")
                }
                
            })
        }
        return
    }
    class func insertPersonTypes()
    {
        dataContext.reset()
        
        if dataContext.personType.count() == 0
        {
            DataManager.getTopAppsDataFromFileWithSuccess("personTypes", { (data) -> Void in
                let json = JSON(data: data)
                
                for index in 0..<json.count
                {
                    var personType = dataContext.personType.createEntity()
                    
                    personType.name = json[index]["name"].string!
                    
                }
                
                if dataContext.save()
                {
                    println("PersonTypes Saved successfully")
                }
                
            })
        }
        return
    }
    
    class func insertPersons()
    {
        dataContext.reset()
        
        if dataContext.person.count() == 0
        {
            DataManager.getTopAppsDataFromFileWithSuccess("persons", { (data) -> Void in
                let json = JSON(data: data)
                
                for index in 0..<json.count
                {
                    var person = dataContext.person.createEntity()
                    
                    person.firstName = json[index]["firstName"].string!
                    person.lastName = json[index]["lastName"].string!
                    person.mobilePhone = json[index]["mobilePhone"].string!
                    person.email = json[index]["email"].string!
                    
                    if dataContext.save()
                    {
                        println("Persons Saved successfully")
                    }
                    
                }
                
                
                
            })
        }
        return
    }

    class func insertAreaUnit()
    {
        dataContext.reset()
        
        if dataContext.areaUnit.count() == 0
        {
            DataManager.getTopAppsDataFromFileWithSuccess("areaUnit", { (data) -> Void in
                let json = JSON(data: data)
                for index in 0..<json.count
                {
                    var areaUnit = dataContext.areaUnit.createEntity()
                    
                    areaUnit.name = json[index]["name"].string!
                    areaUnit.symbol = json[index]["symbol"].string!
                    areaUnit.unitValue = json[index]["unitValue"].doubleValue
                    
                    if dataContext.save()
                    {
                        println("AreaUnit Saved successfully")
                    }
                    
                }
                
                
                
            })
        }
        return
    }

    
}