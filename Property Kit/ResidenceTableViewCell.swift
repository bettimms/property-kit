//
//  ResidenceTableViewCell.swift
//  Property Kit
//
//  Created by Betim S on 1/13/15.
//  Copyright (c) 2015 Betim Sejdiu. All rights reserved.
//

import UIKit

class ResidenceTableViewCell: UITableViewCell
{

    @IBOutlet weak var residenceImageView: UIImageView!
    @IBOutlet weak var resDescriptionLabel: UILabel!
    @IBOutlet weak var resLocationLabel: UILabel!
    @IBOutlet weak var resDateAddedLabel: UILabel!
    @IBOutlet weak var resPriceLabel: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
