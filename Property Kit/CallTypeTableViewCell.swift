//
//  CallTypeTableViewCell.swift
//  Property Kit
//
//  Created by Betim S on 1/17/15.
//  Copyright (c) 2015 Betim Sejdiu. All rights reserved.
//

import UIKit

class CallTypeTableViewCell: UITableViewCell
{
    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var callDescriptionTextField: UITextField!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
