//
//  Person.swift
//  Property Kit
//
//  Created by Betim S on 1/16/15.
//  Copyright (c) 2015 Betim Sejdiu. All rights reserved.
//

import Foundation
import CoreData

class Person: NSManagedObject
{
    
    @NSManaged var email: String
    @NSManaged var firstName: String
    @NSManaged var lastName: String
    @NSManaged var mobilePhone: String
    @NSManaged var callType: CallType
    @NSManaged var personType: PersonType
    @NSManaged var residence: NSSet
    
    var name:String
        {
        //Return firstName and lastName in this format to compare with cell text in UITableViewExtension
        get {
            
            if !self.fault
            {
                return "\(self.firstName)  \(self.lastName)"
            }
            return ""
        }
    }
    
}
