//
//  Property.swift
//  Property Kit
//
//  Created by Betim S on 1/16/15.
//  Copyright (c) 2015 Betim Sejdiu. All rights reserved.
//

import Foundation
import CoreData

class Property: NSManagedObject
{

    @NSManaged var age: NSNumber
    @NSManaged var name: String
    @NSManaged var number: NSNumber
    @NSManaged var type: String
    @NSManaged var city: City
    @NSManaged var residence: NSSet

}
