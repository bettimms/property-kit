//
//  City.swift
//  Property Kit
//
//  Created by Betim S on 1/16/15.
//  Copyright (c) 2015 Betim Sejdiu. All rights reserved.
//

import Foundation
import CoreData

class City: NSManagedObject
{

    @NSManaged var code: String
    @NSManaged var name: String
    @NSManaged var country: Country
    @NSManaged var property: NSSet

}
