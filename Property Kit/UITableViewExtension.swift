//
//  UITableViewExtension.swift
//  Property Kit
//
//  Created by Betim S on 1/24/15.
//  Copyright (c) 2015 Betim Sejdiu. All rights reserved.
//

import Foundation
import CoreData

extension UITableView
{
    //Gets objects from tableView dataSource from the given dataSource (array)
    public func objectsForSelectedRowsFromArray<T>(array:[T])->[T]
    {
        var elements:[T] = []
        if array.count > 0
        {
            let rows = self.numberOfRowsInSection(0)
            
            for i in 0...rows - 1
            {
                var indexPath = NSIndexPath(forRow: i, inSection: 0)
                var cell = self.cellForRowAtIndexPath(indexPath)
                let accessoryType = cell!.accessoryType.rawValue
                
                if cell?.selected == true
                {
                    elements.append(array[i] as T)
                }
            }
            
        }
        return elements
    }
    
    //Gets an object from tableView dataSource from the given dataSource (array)
    public func objectForSelectedRowFromArray<T>(array:[T])->T?
    {
        let rows = self.numberOfRowsInSection(0)
        var element:T?
        
        for i in 0...rows - 1
        {
            var indexPath = NSIndexPath(forRow: i, inSection: 0)
            var cell = self.cellForRowAtIndexPath(indexPath)
            
            if cell?.selected == true
            {
                element = array[i] as T
            }
            
        }
        
        return element
    }
    
    //Selects rows in tableView that contains the given objects
    public func selectRowsWithObjects(objects:[AnyObject])
    {
        let rows = self.numberOfRowsInSection(0)
        
        if objects.count > 0
        {
            for i in 0...objects.count - 1
            {
                let itemName = objects[i].name
                
                for j in 0...rows - 1
                {
                    let indexPath = NSIndexPath(forRow: j, inSection: 0)
                    let cell = self.cellForRowAtIndexPath(indexPath)
                    
                    if itemName == cell!.textLabel!.text!
                    {
                        cell?.accessoryType = UITableViewCellAccessoryType.Checkmark
                        self.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: UITableViewScrollPosition.None)
                        
                    }
                }
            }
        }
        
    }
    
    //Selects a row in tableView that contains the given object
    public func selectRowWithObject(object:AnyObject?)
    {
        let rows = self.numberOfRowsInSection(0)

        if let item: AnyObject = object
        {
            for j in 0...rows - 1
            {
                let indexPath = NSIndexPath(forRow: j, inSection: 0)
                self.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Middle, animated: true)
                //
                let cell = self.cellForRowAtIndexPath(indexPath)
                
                
                if let itemName = item.name
                {
                    if itemName == cell!.textLabel!.text!
                    {
                        cell?.accessoryType = UITableViewCellAccessoryType.Checkmark
                        self.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: UITableViewScrollPosition.None)
                       
                        break
                    }
                }
                //                else
                //                {
                //                    //Special case
                //                    if selectedItem is Person
                //                    {
                //                        let p = selectedItem as Person
                //                        let rowText = cell!.textLabel!.text!.componentsSeparatedByString(" ")
                //
                //                        if p.firstName == rowText[0]
                //                        {
                //                            cell?.accessoryType = UITableViewCellAccessoryType.Checkmark
                //                            self.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: UITableViewScrollPosition.None)
                //                        }
                //                    }
                //                }
                
            }
        }
    }
    
    //On displaying cell on tableView it checks whether cell is selected or no then show the accessoryType
    //Default accessoryType is Checkmark
    public func showAccessoryTypeForSelectedCell(cell:UITableViewCell, accessoryType:UITableViewCellAccessoryType?)
    {
        
        if cell.selected == false
        {
            cell.accessoryType = UITableViewCellAccessoryType.None
        }
        else
        {
            cell.accessoryType = accessoryType ?? UITableViewCellAccessoryType.Checkmark
        }
    }
    
    //Obsolete
    public func deselectAllRows()
    {
        let totalCells = self.numberOfRowsInSection(0)
        println(totalCells)
        for i in 0...totalCells - 1
        {
            let indexPath = NSIndexPath(forRow: i, inSection: 0)
            self.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Middle, animated: false)
            let cell = self.cellForRowAtIndexPath(indexPath)
            cell!.selected = false
            cell!.accessoryType = UITableViewCellAccessoryType.None
        }
    }
    
}