//
//  Property_Kit.swift
//  Property Kit
//
//  Created by Betim S on 1/22/15.
//  Copyright (c) 2015 Betim Sejdiu. All rights reserved.
//

import Foundation
import CoreData

class Residence: NSManagedObject {

    @NSManaged var area: NSNumber
    @NSManaged var dateAdded: NSDate
    @NSManaged var dateModified: NSDate
    @NSManaged var isFurnished: NSNumber
    @NSManaged var kitchenType: String
    @NSManaged var number: String
    @NSManaged var numberOfBathrooms: NSNumber
    @NSManaged var numberOfLoundaries: NSNumber
    @NSManaged var numberOfStoreRooms: NSNumber
    @NSManaged var referenceId: String
    @NSManaged var residenceDescription: String
    @NSManaged var residenceStatus: String
    @NSManaged var areaUnit: AreaUnit
    @NSManaged var facility: NSSet
    @NSManaged var payment: Payment
    @NSManaged var person: Person
    @NSManaged var property: Property
    @NSManaged var residenceType:ResidenceType

}
