//
//  ResidenceTableViewController.swift
//  Property Kit
//
//  Created by Betim S on 1/12/15.
//  Copyright (c) 2015 Betim Sejdiu. All rights reserved.
//

import UIKit

class ResidenceTableViewController: UITableViewController
{
    var residences:[Residence] = []
    var test:String = "WOrking"
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        residences = dataContext.residence.toArray()
        
        //Setup revealing
        let reveal = revealViewController()
        reveal.panGestureRecognizer()
        reveal.tapGestureRecognizer()
        
    }
    
    func reloadData()
    {
        residences = dataContext.residence.toArray()
        tableView.reloadData()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return residences.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCellWithIdentifier("residenceCell", forIndexPath: indexPath) as ResidenceTableViewCell
        cell.resDescriptionLabel.text = residences[indexPath.row].residenceDescription
        
        let res = residences[indexPath.row] as Residence
        cell.resDateAddedLabel.text = String.convertFromStringInterpolationSegment(res.dateAdded)
        cell.resPriceLabel.text = res.payment.price
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "addResidenceIdentifier"
        {
            let navViewController = segue.destinationViewController as UINavigationController
            let addResidenceViewController = navViewController.viewControllers[0] as AddResidenceTableViewController
            addResidenceViewController.residenceTableViewController = self
            
        }
    }
    
}
