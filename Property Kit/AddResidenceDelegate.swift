//
//  AreaDelegate.swift
//  Property Kit
//
//  Created by Betim S on 1/18/15.
//  Copyright (c) 2015 Betim Sejdiu. All rights reserved.
//

import Foundation

@objc
protocol AddResidenceDelegate
{
    optional func didSelectArea(area:String, areaUnit:AreaUnit)
    optional func didSelectResidenceType(residenceType:ResidenceType)
    optional func didSelectPerson(person:Person)
    optional func didSelectPayment(payment:Payment)
    optional func didSelectProperty(property:Property)
    optional func didSelectFacilities(facilities:[Facility])
    
    //For other ViewController
    optional func didSelectCountry(country:Country)
    optional func didSelectPersonType(personType:PersonType)
}
