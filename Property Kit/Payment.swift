//
//  Payment.swift
//  Property Kit
//
//  Created by Betim S on 1/17/15.
//  Copyright (c) 2015 Betim Sejdiu. All rights reserved.
//

import Foundation
import CoreData

class Payment: NSManagedObject
{

    @NSManaged var price: String
    @NSManaged var numberOfCheques: NSNumber
    @NSManaged var deposit: NSNumber
    @NSManaged var agencyFee: NSNumber
    @NSManaged var currency: String
    @NSManaged var residence: Residence

}
