//
//  ResidenceType.swift
//  Property Kit
//
//  Created by Betim S on 1/17/15.
//  Copyright (c) 2015 Betim Sejdiu. All rights reserved.
//

import Foundation
import CoreData

class ResidenceType: NSManagedObject
{

    @NSManaged var name: String
    @NSManaged var residence: Residence

}
